import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testApp/mockData.dart';
import 'package:testApp/Model/DetailsCard.dart';
import 'package:testApp/Widgets/AppWidgets.dart';
import 'package:testApp/Widgets/CardsWidgets.dart';
import 'package:testApp/controller/appController.dart';
import 'package:testApp/resource/Fonts.dart';

class HomePage extends StatelessWidget {
  AppController appController = Get.find();

  WidgetsTrip widgetsTrip = new WidgetsTrip();

  AppWidgets appWidgets = new AppWidgets();

  ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: GetBuilder<AppController>(
        init: appController,
        builder: (_) => Scaffold(
          appBar: appWidgets.appBar(width),
          body: ListView(
            children: [
              Container(
                width: width,
                height: height * 0.18,
                child: Stack(
                  fit: StackFit.loose,
                  children: [
                    Positioned(
                      top: 5,
                      right: 5,
                      child: Text(
                        _.months[_.dateTime.month - 1],
                        style: TextStyle(
                            color: Colors.blueAccent.withOpacity(0.3),
                            fontSize: 55,
                            fontFamily: FontApp.productSans,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        width: width,
                        height: 100,
                        alignment: Alignment.center,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: _.days,
                            itemBuilder: (context, int index) {
                              return widgetsTrip.dayCards(
                                  _.week[index], _.day[index], context, index);
                            }),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: width,
                height: 250,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 7,
                    itemBuilder: (context, int index) {
                      return widgetsTrip.arrivalPlan(
                        index,
                        index.toString(),
                        "title",
                        "description",
                        "assets/imgs/car.png",
                        "8",
                        "We",
                        Colors.red,
                        Colors.redAccent,
                      );
                    }),
              ),
              widgetsTrip.detailsCardTrip(
                  _.details[0].header, _.details[0].cards, context),
              widgetsTrip.detailsCardTrip(
                  _.details[1].header, _.details[1].cards, context),
              widgetsTrip.detailsCardTrip(
                  _.details[2].header, _.details[2].cards, context),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Text(
                          "Your Hotel",
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: FontApp.productSans,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: width * 0.9,
                    height: 150,
                    margin: EdgeInsets.only(bottom: 20, top: 10),
                    child: widgetsTrip.hotelCard(
                        "Hotel Atlantis",
                        5,
                        "Amanda Tower 2 - shelkh Zayed \n Rd - Jumeirah Lakes Tower",
                        "assets/imgs/house/house2.jpg",
                        width,
                        context),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
