import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testApp/MockData.dart';
import 'package:testApp/Model/Archievements.dart';
import 'package:testApp/Model/History.dart';
import 'package:testApp/Model/person.dart';
import 'package:testApp/Widgets/AppWidgets.dart';
import 'package:testApp/Widgets/CardsWidgets.dart';
import 'package:testApp/controller/appController.dart';
import 'package:testApp/resource/Fonts.dart';

class ProfilePage extends StatelessWidget {
  WidgetsTrip widgetsTrip = new WidgetsTrip();

  AppController appController = Get.find();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GetBuilder<AppController>(
      init: appController,
      builder: (_) => Scaffold(
        body: ListView(
          children: [
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () {},
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "Edit Profile",
                          style: TextStyle(
                              fontFamily: FontApp.productSans,
                              fontWeight: FontWeight.w300,
                              color: Colors.blueGrey),
                        ),
                      ),
                      Icon(
                        Icons.settings,
                        color: Colors.blueGrey,
                      )
                    ],
                  ),
                )
              ],
            ),
            Row(
              children: [
                Container(
                  width: 45,
                  height: 45,
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    child: Image.asset(
                      _.user.image,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      _.user.name,
                      style: TextStyle(
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.bold,
                          fontSize: 22),
                    ),
                    Text(
                      "Born on ${_.user.bDay}",
                      style: TextStyle(
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    )
                  ],
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                children: [
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.blueGrey.withOpacity(0.5),
                              blurRadius: 5,
                              spreadRadius: 1,
                              offset: Offset(5, 5))
                        ]),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      child: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTGJvsxZhtf6bApXEjpWh8VPYN8TloloFDdXA&usqp=CAU",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      _.user.country,
                      style: TextStyle(
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.w400,
                          fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.phone,
                    color: Colors.blueAccent,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              _.user.phoneNumber,
                              style: TextStyle(
                                fontFamily: FontApp.productSans,
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                              ),
                            ),
                            _.user.verify == true
                                ? SizedBox()
                                : Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                  ),
                          ],
                        ),
                        Text(
                          _.user.verify == true
                              ? "Verified"
                              : "Phone number is not verified",
                          style: TextStyle(
                            color: _.user.verify == true
                                ? Colors.green
                                : Colors.red,
                          ),
                        )
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 60,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 15,
                            color: Colors.blueAccent.withOpacity(0.5),
                            spreadRadius: 3,
                            offset: Offset(10, 5),
                          )
                        ],
                        gradient: LinearGradient(colors: [
                          Colors.lightBlueAccent,
                          Colors.blueAccent,
                        ]),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Text(
                      "Verify",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.w300,
                          fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: width,
              height: 120,
              child: Stack(
                fit: StackFit.loose,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: width * 0.9,
                      height: 80,
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          gradient: LinearGradient(
                            colors: [
                              Colors.blueAccent.withOpacity(0.7),
                              Colors.blue,
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.blueAccent.withOpacity(0.5),
                                blurRadius: 15,
                                spreadRadius: 2,
                                offset: Offset(5, 5))
                          ]),
                      child: Row(
                        children: [
                          Text(
                            "Total Coins",
                            style: TextStyle(
                              fontFamily: FontApp.productSans,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          Spacer(),
                          Text(
                            _.user.coins,
                            style: TextStyle(
                              fontFamily: FontApp.productSans,
                              fontWeight: FontWeight.bold,
                              color: Colors.amber,
                              fontSize: 30,
                            ),
                          ),
                          Container(
                            width: 40,
                            height: 40,
                            padding: EdgeInsets.all(5),
                            child: Image.asset("assets/imgs/money.png"),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 60,
                      child: Container(
                        width: 110,
                        height: 40,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blueAccent.withOpacity(0.3),
                              spreadRadius: 2,
                              blurRadius: 15,
                              offset: Offset(0,10)
                            )
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(25))
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "View Offers",
                              style: TextStyle(
                                fontFamily: FontApp.productSans,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            Icon(Icons.arrow_forward_ios,
                            color: Colors.black,
                            size: 12,),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Your Achievements",
                    style: TextStyle(
                      fontFamily: FontApp.opumMai,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    width: 60,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 15,
                            color: Colors.blueAccent.withOpacity(0.5),
                            spreadRadius: 3,
                            offset: Offset(10, 5),
                          )
                        ],
                        gradient: LinearGradient(colors: [
                          Colors.lightBlueAccent,
                          Colors.blueAccent,
                        ]),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Text(
                      "View All",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.w300,
                          fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: width,
              height: 120,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.all(10),
                  itemCount: _.archiev.length,
                  itemBuilder: (context, int index) {
                    return widgetsTrip.archievement(
                        _.archiev[index].archiv, _.archiev[index].image);
                  }),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Text(
                "History",
                style: TextStyle(
                    fontFamily: FontApp.opumMai,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Container(
              width: width,
              height: 300,
              child: ListView.builder(
                  itemCount: _.history.length,
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.all(0),
                  itemBuilder: (context, int index) {
                    return widgetsTrip.history(
                        _.history[index].image,
                        _.history[index].title,
                        _.history[index].desc,
                        _.history[index].beginDate,
                        _.history[index].endDate,
                        _.history[index].month,
                        _.history[index].year,
                        index,
                        _.history.length);
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
