class Person {
  String name;
  String image;
  String bDay;
  String country;
  String phoneNumber;
  String coins;
  bool verify;

  Person();

  Person.fromJson(Map<String, dynamic> json) {
    try {
      this.name = json['name'];
      this.image = json['image'];
      this.bDay = json['bDay'];
      this.coins = json['coins'];
      this.country = json['country'];
      this.phoneNumber = json['phoneNumber'];
      this.verify = json['verify'];
    } catch (e) {
      print(e);
    }
  }
}
