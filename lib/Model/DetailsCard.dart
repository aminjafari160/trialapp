import 'package:testApp/Model/CardTrip.dart';

class DetailsCard {
  String header;
  List<CardTrip> cards;

  DetailsCard();

  DetailsCard.fromJson(Map<String, dynamic> json) {
    try {
      this.header = json['header'] != null ? json['header'] : null;
      this.cards = json['cards'] != null
          ? List.from(json['cards']).map((e) => CardTrip.fromJson(e)).toList()
          : [];
    } catch (e) {
      print(e);
    }
  }

}


