class History {
  String image;
  String title;
  String desc;
  String beginDate;
  String endDate;
  String month;
  String year;

  History();

  History.fromJson(Map<String, dynamic> json) {
    try {
      this.image = json['image'];
      this.title = json['title'];
      this.desc = json['description'];
      this.beginDate = json['begin'];
      this.endDate = json['end'];
      this.month = json['month'];
      this.year = json['year'];
    } catch (e) {
      print(e);
    }
  }
}
