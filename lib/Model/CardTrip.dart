class CardTrip {
  String title;
  String price;
  String image;

  CardTrip();

  CardTrip.fromJson(Map<String, dynamic> json) {
    try {
      this.title = json['title'] != null ? json['title'] : null;
      this.price = json['price'] != null ? json['price'] : null;
      this.image = json['image'] != null ? json['image'] : null;
    } catch (e) {
      print(e);
    }
  }

  Map toMap() {
    Map json = new Map();
    json['title'] = this.title;
    json['price'] = this.price;
    json['image'] = this.image;
  }
}