import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:testApp/MockData.dart';
import 'package:testApp/Model/Archievements.dart';
import 'package:testApp/Model/DetailsCard.dart';
import 'package:testApp/Model/History.dart';
import 'package:testApp/Model/person.dart';

class AppController extends GetxController {
  // varibales
  var user = Person.fromJson(person);

  List<Archievement> archiev =
      List.from(mockDataArchiev).map((e) => Archievement.fromJson(e)).toList();

  List<History> history =
      List.from(mockDataHistory).map((e) => History.fromJson(e)).toList();

  List<DetailsCard> details =
      List.from(mockDataDetails).map((e) => DetailsCard.fromJson(e)).toList();

  DateTime _dateTime;
  List _months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  List _week = ['Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr'];

  List _day = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  PageController _pageController = new PageController();

  int _days = 7;

  int _itemSelect = 3;

  int _pageSelect = 0;

  // list of getter
  PageController get pageController => _pageController;

  int get pageSelect => _pageSelect;

  List get day => _day;

  int get itemSelect => _itemSelect;

  int get days => _days;

  List get week => _week;

  List get months => _months;

  DateTime get dateTime => DateTime.now();

  // list of setter
  set pageController(PageController pageController) {
    _pageController = pageController;
  }

  set pageSelect(int pageSelect) {
    _pageSelect = pageSelect;
    update();
  }

  set day(List day) {
    _day = day;
  }

  set itemSelect(int itemSelect) {
    _itemSelect = itemSelect;
    update();
  }

  set days(int days) {
    _days = days;
  }

  set months(List months) {
    _months = months;
  }

  set week(List week) {
    _week = week;
  }

  set dateTime(DateTime dateTime) {
    _dateTime = dateTime;
  }

  //functions

}
