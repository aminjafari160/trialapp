import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testApp/controller/appController.dart';
import 'package:testApp/resource/Fonts.dart';

class AppWidgets {
  AppController appController = Get.find();

  Widget bottomNavBar() {
    return GetBuilder<AppController>(
      init: appController,
      builder: (_) => BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: _.pageSelect == 0
                    ? Colors.lightBlueAccent
                    : Colors.blueGrey,
              ),
              title: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: _.pageSelect == 0
                          ? LinearGradient(colors: [
                              Colors.lightBlueAccent,
                              Colors.blueAccent,
                            ])
                          : null),
                  child: Text(
                    'Home',
                    style: TextStyle(
                        fontFamily: FontApp.productSans,
                        color:
                            _.pageSelect == 0 ? Colors.white : Colors.blueGrey,
                        fontWeight: _.pageSelect == 0 ? FontWeight.bold : null,
                        fontSize: _.pageSelect == 0 ? 12 : 10),
                  ))),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.notifications,
                color: _.pageSelect == 1
                    ? Colors.lightBlueAccent
                    : Colors.blueGrey,
              ),
              title: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: _.pageSelect == 1
                          ? LinearGradient(colors: [
                              Colors.lightBlueAccent,
                              Colors.blueAccent,
                            ])
                          : null),
                  child: Text(
                    'Notifications',
                    style: TextStyle(
                        fontFamily: FontApp.productSans,
                        color: _.pageSelect == 1 ? Colors.white : Colors.black,
                        fontWeight: _.pageSelect == 1 ? FontWeight.bold : null,
                        fontSize: _.pageSelect == 1 ? 12 : 10),
                  ))),
          BottomNavigationBarItem(
            icon: Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(50)),
                child: Image.asset(
                  "assets/imgs/man.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            title: Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              margin:
                  _.pageSelect == 2 ? EdgeInsets.symmetric(vertical: 2) : null,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  gradient: _.pageSelect == 2
                      ? LinearGradient(colors: [
                          Colors.lightBlueAccent,
                          Colors.blueAccent,
                        ])
                      : null),
              child: Text(
                'LC',
                style: TextStyle(
                    fontFamily: FontApp.productSans,
                    color: _.pageSelect == 2 ? Colors.white : Colors.black,
                    fontWeight: _.pageSelect == 2 ? FontWeight.bold : null,
                    fontSize: _.pageSelect == 2 ? 12 : 10),
              ),
            ),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.more_vert,
                color: _.pageSelect == 3
                    ? Colors.lightBlueAccent
                    : Colors.blueGrey,
              ),
              title: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: _.pageSelect == 3
                          ? LinearGradient(colors: [
                              Colors.lightBlueAccent,
                              Colors.blueAccent,
                            ])
                          : null),
                  child: Text(
                    'More',
                    style: TextStyle(
                        fontFamily: FontApp.productSans,
                        color:
                            _.pageSelect == 3 ? Colors.white : Colors.blueGrey,
                        fontWeight: _.pageSelect == 3 ? FontWeight.bold : null,
                        fontSize: _.pageSelect == 3 ? 12 : 10),
                  ))),
        ],
        currentIndex: _.pageSelect,
        fixedColor: Colors.deepPurple,
        onTap: (value) {
          _.pageSelect = value;
          appController.pageController.animateToPage(value,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        },
      ),
    );
  }

  Widget appBar(double width) {
    return PreferredSize(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(icon: Icon(Icons.menu), onPressed: () {}),
                Spacer(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Dubai Trip plan",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontApp.productSans,
                          fontSize: 25,
                        ),
                      ),
                      Container(
                        // color: Colors.amber,
                        height: 3,
                        width: width * 0.4,
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        child: Divider(
                          height: 3,
                          thickness: 3.0,
                          color: Colors.amber,
                        ),
                      ),
                      Container(
                        width: width * 0.4,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 20,
                              height: 20,
                              child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  child: Image.network(
                                      "https://asreertebat.com/picture/review_01-03-2018-1514984985.png")),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(
                                "Alibaba Group",
                                style: TextStyle(
                                  fontFamily: FontApp.productSans,
                                  color: Colors.black54,
                                  fontSize: 10,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  InkWell(
                    child: Container(
                      width: 35,
                      height: 35,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 5,
                                spreadRadius: 1,
                                color: Colors.blueAccent.withOpacity(0.5),
                                offset: Offset(5, 5))
                          ],
                          gradient: LinearGradient(colors: [
                            Colors.lightBlueAccent,
                            Colors.blueAccent,
                          ])),
                      child: Icon(
                        Icons.map,
                        color: Colors.white,
                        size: 18,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        preferredSize: Size(width, 100));
  }
}
