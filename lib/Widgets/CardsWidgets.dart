import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:testApp/Model/CardTrip.dart';
import 'package:testApp/Model/DetailsCard.dart';
import 'package:testApp/controller/appController.dart';
import 'package:testApp/resource/Fonts.dart';

class WidgetsTrip {
  AppController appController = Get.find();

  Widget dayCards(
      String weekDay, int dateDay, BuildContext context, int index) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          index == 3
              ? Container(
                  width: 45,
                  height: 15,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  child: Text(
                    "Today",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: FontApp.productSans,
                        fontSize: 10),
                  ),
                )
              : SizedBox(),
          GestureDetector(
            onTap: () {
              if (index != appController.itemSelect) {
                appController.itemSelect = index;
              }
            },
            child: Container(
              width: 60,
              height: 60,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    index == appController.itemSelect
                        ? BoxShadow(
                            color: Colors.blueAccent.withOpacity(0.4),
                            blurRadius: 5,
                            spreadRadius: 2,
                            offset: Offset(4, 4),
                          )
                        : BoxShadow(color: Colors.transparent)
                  ],
                  color: index == appController.itemSelect
                      ? Colors.white
                      : Color(0xffeeeeee),
                  border: index == appController.itemSelect
                      ? Border.all(color: Colors.blueAccent, width: 1)
                      : null),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(dateDay.toString(),
                      style: TextStyle(
                        fontFamily: FontApp.productSans,
                        color: index == appController.itemSelect
                            ? Colors.blue
                            : Colors.black87,
                        fontWeight: FontWeight.bold,
                      )),
                  Text(
                    weekDay,
                    style: TextStyle(
                        fontFamily: FontApp.productSans,
                        color: index == appController.itemSelect
                            ? Colors.blue
                            : Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget detailsCardTrip(
      String header, List<CardTrip> cards, BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                header,
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: FontApp.productSans,
                  fontWeight: FontWeight.bold,
                ),
              ),
              GestureDetector(
                onTap: () {
                  // show all Items
                },
                child: Container(
                  width: 50,
                  height: 15,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Colors.lightBlueAccent,
                        Colors.blueAccent,
                      ]),
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Text(
                    "View all",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: FontApp.productSans,
                        fontWeight: FontWeight.w300,
                        fontSize: 10),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 150,
          child: ListView.builder(
              itemCount: cards.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, int index) {
                return cardsTrip(
                    cards[index].title, cards[index].price, cards[index].image);
              }),
        )
      ],
    );
  }

  Widget cardsTrip(String title, String price, String image) {
    return Container(
      width: 110,
      height: 170,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: Stack(
        fit: StackFit.loose,
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 110,
              height: 170,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: Image.asset(
                  image,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: 110,
              height: 170,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  gradient: LinearGradient(
                      colors: [Colors.black45, Colors.transparent],
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter)),
            ),
          ),
          Positioned(
            left: 5,
            bottom: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontFamily: FontApp.productSans,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "From $price \u0024",
                  style: TextStyle(
                    fontFamily: FontApp.productSans,
                    color: Colors.white,
                    fontSize: 12,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget hotelCard(String hotelName, int stars, String address, String image,
      double width, BuildContext context) {
    return Stack(
      fit: StackFit.loose,
      children: [
        Align(
          alignment: Alignment.center,
          child: Container(
            width: width * 0.9,
            height: 150,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              child: Image.asset(
                image,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            width: width * 0.9,
            height: 150,
            decoration: BoxDecoration(
              color: Colors.blueAccent.withOpacity(0.6),
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
          ),
        ),
        Positioned(
            top: 15,
            bottom: 15,
            left: 10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  hotelName,
                  style: TextStyle(
                      fontFamily: FontApp.productSans, color: Colors.white),
                ),
                Container(
                  width: width * 0.5,
                  height: 30,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: stars,
                      itemBuilder: (context, int index) {
                        return Icon(
                          Icons.star,
                          color: Colors.white,
                          size: 14,
                        );
                      }),
                ),
                Text(
                  address,
                  style: TextStyle(
                    fontFamily: FontApp.productSans,
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                    fontSize: 14,
                  ),
                )
              ],
            ))
      ],
    );
  }

  Widget arrivalPlan(int index, String hours, String title, String description,
      String image, String day, String weekDay, Color color1, Color color2) {
    return index == 0
        ? Container(
            width: 100,
            height: 250,
            padding: EdgeInsets.symmetric(horizontal: 10),
            alignment: Alignment.center,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      day,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      weekDay,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: FontApp.productSans,
                          fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Details >>",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: FontApp.productSans,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
                Container(
                  width: 2,
                  height: 120,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                    colors: [
                      Colors.transparent,
                      Colors.black,
                      Colors.black,
                      Colors.transparent,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  )),
                )
              ],
            ),
          )
        : Container(
            width: 200,
            height: 250,
            child: Stack(
              fit: StackFit.loose,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                            color: color1.withOpacity(0.7),
                            blurRadius: 10,
                            offset: Offset(5, 5),
                            spreadRadius: 2)
                      ],
                      gradient: LinearGradient(
                        colors: [
                          color2,
                          color1,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Spacer(),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              margin: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                  color: Colors.white),
                              child: Text(
                                "$hours hour",
                                style: TextStyle(
                                  color: Colors.redAccent,
                                  fontFamily: FontApp.productSans,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          title,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: FontApp.productSans,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 100,
                          padding: EdgeInsets.all(5),
                          alignment: Alignment.center,
                          child: Text(
                            description,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: FontApp.productSans,
                                fontWeight: FontWeight.w300),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                    left: -5,
                    bottom: 2,
                    child: Container(
                      width: 120,
                      height: 100,
                      child: Image.asset(
                        image,
                        fit: BoxFit.fill,
                      ),
                    ))
              ],
            ),
          );
  }

  Widget archievement(String archievement, String image) {
    return Column(
      children: [
        Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(8),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Colors.blueAccent,
              width: 2,
            ),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(50)),
            child: Image.asset(
              image,
              fit: BoxFit.contain,
            ),
          ),
        ),
        Text(
          archievement,
          style: TextStyle(
              fontFamily: FontApp.productSans,
              color: Colors.blueGrey,
              fontWeight: FontWeight.w400,
              fontSize: 16),
        )
      ],
    );
  }

  Widget history(String image, String title, String desc, String beginDate,
      String endDate, String month, String year, int index, int lenght) {
    return index == 0
        ? Container(
            alignment: Alignment.center,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 20,
                  height: 1,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.transparent,
                      Colors.blueGrey,
                    ]),
                  ),
                ),
                Text(
                  year,
                  style: TextStyle(
                      fontFamily: FontApp.opumMai,
                      fontWeight: FontWeight.w300,
                      fontSize: 12),
                ),
                Container(
                  width: 20,
                  height: 1,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.blueGrey,
                      Colors.transparent,
                    ]),
                  ),
                ),
              ],
            ),
          )
        : Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Colors.blueGrey.withOpacity(0.2),
                  ),
                  child: Text(
                    month,
                    style: TextStyle(
                        fontFamily: FontApp.opumMai,
                        fontWeight: FontWeight.w200,
                        fontSize: 10),
                  ),
                ),
                (index % 2 == 0 || index % 2 != 0) && index != lenght - 1
                    ? Container(
                        width: 1,
                        height: 150,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                          Colors.transparent,
                          Colors.blueGrey
                        ])),
                      )
                    : SizedBox(),
              ],
            ),
            Container(
              width: 250,
              height: 150,
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    spreadRadius: 2,
                    color: Colors.blueGrey.withOpacity(0.4),
                    offset: Offset(10, 5))
              ]),
              child: Stack(
                fit: StackFit.loose,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 250,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        child: Image.asset(
                          image,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 250,
                      height: 150,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Colors.transparent,
                            Colors.blueGrey.withOpacity(0.8),
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 10,
                    right: 10,
                    top: 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                              fontFamily: FontApp.opumMai,
                              fontWeight: FontWeight.w200,
                              color: Colors.white,
                              fontSize: 18),
                        ),
                        Container(
                          width: 30,
                          height: 30,
                          padding: EdgeInsets.all(6),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.amber,
                                Colors.orange,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                          ),
                          child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              child: Image.network(
                                  "https://asreertebat.com/picture/review_01-03-2018-1514984985.png")),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      left: 10,
                      bottom: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            desc,
                            style: TextStyle(
                                fontFamily: FontApp.productSans,
                                fontWeight: FontWeight.w200,
                                color: Colors.white,
                                fontSize: 10),
                          ),
                          Text(
                            "$beginDate - $endDate",
                            style: TextStyle(
                                fontFamily: FontApp.productSans,
                                fontWeight: FontWeight.w200,
                                color: Colors.white,
                                fontSize: 12),
                          )
                        ],
                      ))
                ],
              ),
            )
          ],
        );
  }
}
