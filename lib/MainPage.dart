import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testApp/Pages/HomePage.dart';
import 'package:testApp/Pages/MorePage.dart';
import 'package:testApp/Pages/Notifications.dart';
import 'package:testApp/Pages/ProfilePage.dart';
import 'package:testApp/Widgets/AppWidgets.dart';
import 'package:testApp/Widgets/CardsWidgets.dart';
import 'package:testApp/controller/appController.dart';

class MainPage extends StatelessWidget {
  AppController appController = Get.put(AppController());

  WidgetsTrip widgetsTrip = new WidgetsTrip();

  AppWidgets appWidgets = new AppWidgets();

  PageController pageController;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: GetBuilder<AppController>(
        init: appController,
        builder: (_) => Scaffold(
          body: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _.pageController,
            scrollDirection: Axis.horizontal,
            children: [
              HomePage(),
              NotificationsPage(),
              ProfilePage(),
              MorePage(),
            ],
          ),
          bottomNavigationBar: appWidgets.bottomNavBar(),
        ),
      ),
    );
  }
}
