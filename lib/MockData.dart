List<Map<String, dynamic>> mockDataDetails = [
  {
    "header": "Beacause you're in Dubai",
    "cards": [
      {
        "title": "Safari",
        "price": "39",
        "image": "assets/imgs/city/antalya.jpg",
      },
      {
        "title": "Aquarium",
        "price": "30",
        "image": "assets/imgs/city/barcelona.jpg",
      },
      {
        "title": "Mosque",
        "price": "35",
        "image": "assets/imgs/city/dubai.jpg",
      },
      {
        "title": "Monaco",
        "price": "50",
        "image": "assets/imgs/city/monaco.jpg",
      },
    ],
  },
  {
    "header": "Dubai night",
    "cards": [
      {
        "title": "Sightseeing",
        "price": "45",
        "image": "assets/imgs/city/torento.jpg",
      },
      {
        "title": "Dinner Cruise",
        "price": "30",
        "image": "assets/imgs/house/house1.jpg",
      },
      {
        "title": "Cockto",
        "price": "35",
        "image": "assets/imgs/house/house2.jpg",
      },
      {
        "title": "Monaco",
        "price": "50",
        "image": "assets/imgs/city/monaco.jpg",
      },
    ],
  },
  {
    "header": "For desert lovers",
    "cards": [
      {
        "title": "Sand walk",
        "price": "39",
        "image": "assets/imgs/house/house3.jpg",
      },
      {
        "title": "Sahara",
        "price": "30",
        "image": "assets/imgs/house/house4.jpg",
      },
      {
        "title": "Mosque",
        "price": "35",
        "image": "assets/imgs/house/house5.jpg",
      },
      {
        "title": "Monaco",
        "price": "50",
        "image": "assets/imgs/house/house6.jpg",
      },
    ],
  },
];

List<Map<String, dynamic>> mockDataArchiev = [
  {
    "archievement": "Party man",
    "image": "assets/imgs/party.png",
  },
  {
    "archievement": "Swimmer",
    "image": "assets/imgs/swimer.png",
  }
];

List<Map<String, dynamic>> mockDataHistory = [
  {
    "image": "assets/imgs/city/antalya.jpg",
    "title": "Antalya",
    "description": "Alibaba",
    "begin": "21 Jan",
    "end": "3 May 2020",
    "month": "Jan",
    "year": "2020",
  },
  {
    "image": "assets/imgs/city/barcelona.jpg",
    "title": "Barcelona",
    "description": "Tourradar",
    "begin": "11 Dec",
    "end": "3 Jan 2020",
    "month": "Jan",
    "year": "2020",
  },
  {
    "image": "assets/imgs/city/dubai.jpg",
    "title": "Dubai",
    "description": "Alibaba",
    "begin": "21 Jan",
    "end": "3 May 2020",
    "month": "Jan",
    "year": "2020",
  },
  {
    "image": "assets/imgs/city/monaco.jpg",
    "title": "Monaco",
    "description": "Tourradar",
    "begin": "21 Jan",
    "end": "3 May 2020",
    "month": "Jan",
    "year": "2020",
  },
];

Map<String, dynamic> person = {
  "name": "John Murphy",
  "image": "assets/imgs/man.png",
  "coins" : "300",
  "bDay": "11/05/1987",
  "country": "England",
  "phoneNumber": "+1 222 4569",
  "verify": true,
};
